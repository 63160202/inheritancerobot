/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author admin
 */
public class Fuel extends Obj{
    private int Volume;
    public Fuel (int x , int y ,int Volume){
        super('F',x,y);
        this.Volume = Volume;
    }
    
    public int fillFuel(){
        int fill = this.Volume;
        this.Volume=0;
        symbol='-';
        return fill;
    }
}
