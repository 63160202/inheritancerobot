/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map,20);
        Tree tree = new Tree (9,5);
        Bomb bomb = new Bomb(5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        map.setTree(tree);
        map.addObj( new Fuel (6,7,20));
        map.addObj( new Fuel (8,10,20));
        map.addObj( new Fuel (20,9,20));
        map.addObj( new Tree (20,20));
        map.addObj( new Tree (20,20));
        map.addObj( new Tree (19,19));
        map.addObj( new Tree (17,16));
        map.addObj( new Tree (20,2));
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
